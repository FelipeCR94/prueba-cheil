const cors = require('cors');
const express = require('express');
const path = require('path');
const app = express();
const rutas = require('./routes/index');
const rutasTareas = require('./routes/tareas');
//const rutaJson = './../images-data/data.json';

//configuraciones
app.set('views', path.join(__dirname, 'views'));
app.set('port', process.env.PORT || 3000);
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

//funciones previas
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

//rutas
app.use(rutas);
app.use('/api',rutasTareas);

app.listen(app.get('port'), () =>{
  console.log('funciona el puerto ', app.get('port'));
})
