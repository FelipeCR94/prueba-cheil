const cors = require('cors');
const express = require('express');
const path = require('path');
const app = express();
const fs = require('fs');
const url = './images-data/data/data.json';
const router = require('express').Router();
var obj;
//const rutas = require('./routes/index');

//configuraciones
app.set('views', path.join(__dirname, 'views'));
app.set('port', process.env.PORT || 3000);

//funciones previas
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

/**/
function porId(elId){
  return elId.id === '208599';
}
/**/


//funcionalidades
app.get('/', function(req, res) {
  //muestra toda la data json
  fs.readFile(url, function(err, data){
    if (err) throw err;
    obj = JSON.parse(data);
    //res.json(obj);
    res.json(obj.find(porId));
  })
});



app.listen(app.get('port'), () =>{
  console.log('funciona el puerto ', app.get('port'));
})
